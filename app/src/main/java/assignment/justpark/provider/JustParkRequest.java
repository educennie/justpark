package assignment.justpark.provider;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.io.IOException;
import java.net.HttpURLConnection;

import assignment.justpark.Configuration;
import assignment.justpark.utils.FileUtils;

/**
 * Created by ecnieves on 23/04/2015.
 */
abstract class JustParkRequest<T> {

    public static final int HTTP_RESPONSE_OK = 200;

    protected Context context;

    public JustParkRequest(Context context) {
        this.context = context;
    }

    abstract T getDataProcessed(String httpResponse) throws DataProcessedException;

    void performRequest(final ResponseCallback<T> responseCallback) {

        new Thread() {
            @Override
            public void run() {

                try {
                    // Perform http request
                    String httpResponse = performHttpRequest(context, Configuration.URL_LOCATION);

                    if(httpResponse == null) {
                        throw new NullPointerException("Invalid http response");
                    }

                    //Get data processed from implementation
                    final T object = getDataProcessed(httpResponse);

                    if(object == null) {
                        throw new NullPointerException("Error data processed");
                    }

                    // Call success callback in foreground thread
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            responseCallback.success(object);
                        }
                    });

                } catch (final Exception exception) {
                    // Call failure callback in foreground thread
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            responseCallback.failure(exception);
                        }
                    });
                }
            }
        }.start();
    }

    private static String performHttpRequest(Context context, String uri) throws IOException {

        java.net.URL url = new java.net.URL(uri);

        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

        if (urlConnection.getResponseCode() == HTTP_RESPONSE_OK) {

            String body = FileUtils.readStream(urlConnection.getInputStream());

            urlConnection.disconnect();

            return body;

        } else {
            urlConnection.disconnect();

            throw new IOException("Response code: " + urlConnection.getResponseCode());
        }
    }
}

