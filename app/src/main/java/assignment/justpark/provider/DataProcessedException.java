package assignment.justpark.provider;

/**
 * Created by educennie on 4/23/15.
 */
public class DataProcessedException extends Exception {
    public DataProcessedException() {
    }

    public DataProcessedException(String detailMessage) {
        super(detailMessage);
    }

    public DataProcessedException(Throwable throwable) {
        super(throwable);
    }
}
