package assignment.justpark.ui.activities;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import assignment.justpark.R;
import assignment.justpark.ui.fragments.ListParkingFragment;
import assignment.justpark.ui.fragments.MapParkingFragment;


public class MainActivity extends ActionBarActivity {

    private final static int FRAGMENT_MAP                = 0;
    private final static int FRAGMENT_LIST               = 1;

    private Button buttonMap;
    private Button buttonList;
    private int currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        buttonMap = (Button)findViewById(R.id.bt_map);
        buttonMap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentFragment != FRAGMENT_MAP) {
                    replaceFragment(MapParkingFragment.newInstance());
                    currentFragment = FRAGMENT_MAP;
                    buttonMap.setSelected(true);
                    buttonList.setSelected(false);
                }
            }
        });

        buttonList = (Button)findViewById(R.id.bt_list);
        buttonList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentFragment != FRAGMENT_LIST) {
                    replaceFragment(ListParkingFragment.newInstance());
                    currentFragment = FRAGMENT_LIST;
                    buttonList.setSelected(true);
                    buttonMap.setSelected(false);
                }
            }
        });

        setDefaultState();

    }

    // Set default state of the screen
    private void setDefaultState() {
        buttonMap.setSelected(true);
        currentFragment = FRAGMENT_MAP;
        addFragment(MapParkingFragment.newInstance());

    }

    public void replaceFragment(Fragment fragment) {
        FragmentTransaction mfFragmentTransaction = getFragmentManager().beginTransaction();
        mfFragmentTransaction.replace(R.id.fragment_container, fragment).commit();
    }

    public void addFragment(Fragment fragment) {
        addFragment(fragment, false);
    }

    public void addFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.fragment_container, fragment);
        if (addToBackStack) {
            fragmentTransaction.addToBackStack(null);
        }
        fragmentTransaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
