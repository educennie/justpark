package assignment.justpark.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import assignment.justpark.R;
import assignment.justpark.model.JPLocation;
import assignment.justpark.provider.JustParkRequestParking;
import assignment.justpark.provider.ResponseCallback;
import assignment.justpark.utils.LocationUtils;

/**
 * Created by educennie on 4/24/15.
 */
public class MapParkingFragment extends Fragment{

    private JPMapFragment mapFragment;

    public static MapParkingFragment newInstance() {
        MapParkingFragment fragment = new MapParkingFragment();

        // Set arguments.
        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);


        final View view = inflater.inflate(R.layout.fragment_map_parking, container, false);

        mapFragment = (JPMapFragment)getChildFragmentManager().findFragmentById(R.id.jpmap);
        if (LocationUtils.checkProviders(getActivity())) {
            mapFragment.enableLocationMap();
        }


        final Button button = (Button) view.findViewById(R.id.buttonPerformLocationsRequest);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO: perform locations request
                button.setEnabled(false);
                new JustParkRequestParking(getActivity())
                        .performRequest(new ResponseCallback<JPLocation>() {
                            @Override
                            public void success(JPLocation object) {
                                button.setEnabled(true);
                                mapFragment.addMarkersAround(object.getData());
                                mapFragment.moveCameraToPosition(new LatLng(Double.parseDouble(object.getCoords().getLat()),
                                        Double.parseDouble(object.getCoords().getLng())));
                                Toast.makeText(getActivity(), "Locations: " + object.getData().size(), Toast.LENGTH_LONG).show();
                            }

                            @Override
                            public void failure(Exception exception) {
                                button.setEnabled(true);
                                Toast.makeText(getActivity(), "Request failed. Message: " + exception.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
            }
        });
        return view;
    }
}
