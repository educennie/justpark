package assignment.justpark.model;

import java.util.List;

/**
 * Created by ecnieves on 23/04/2015.
 */
public class JPLocation {

    /**
     * data : [{"spaces_to_rent":2,"distance":{"km":0.27300765921247,"mile":0.16963909462998},"display_price":{"formatted_price":"£25.00","period":"day","price":25},"available":true,"title":"Driveway on Oakington Manor Drive, HA9","photos":{"user_added":[{"created_at":"2014-06-03 22:00:58","pid":59940,"is_verified":1,"deleted_at":null,"json_data":{"small":{"width":350,"url":"/media/uploaded/listing-photos/538e45c089fec-small.jpg","height":350},"normal":{"width":800,"url":"/media/uploaded/listing-photos/538e45c089fec-normal.jpg","height":600},"square":{"width":50,"url":"/media/uploaded/listing-photos/538e45c089fec-square.jpg","height":50},"thumb":{"width":120,"url":"/media/uploaded/listing-photos/538e45c089fec-thumb.jpg","height":120}},"uid":null,"review_date":"2014-06-04 15:07:20","original_filename":"/media/uploaded/raw/IMG_0128.JPG","is_deleted":0,"updated_at":null,"for_review":0,"id":21503,"timestamp":"2014-06-04 15:07:20"}],"google_map":"https://maps.googleapis.com/maps/api/staticmap?size=640x640&center=51.553457%2C-0.280456&sensor=true&key=AIzaSyB2bEpYuQa2bDYdfP14jJb4qLCwO2SBm24&zoom=15&maptype=roadmap&visual_refresh=true&markers=51.553457%2C-0.280456","google_streetview":"/media/uploaded/listing-photos/538e45c089fec-square.jpg"},"feedback":{"rating":4.78,"count":16},"currency":{"symbol":"£","code":"GBP"},"href":"/1.1/listings/oakington-manor-drive-wembley-ha9-p538e44da823c8/","instant_bookings":1,"category":null,"facilities":["Overnight parking"],"is_api_enabled":0,"coords":{"lng":-0.280456,"lat":51.553457}}]
     * coords : {"lng":"-0.27959400","lat":"51.55585300"}
     */
    private List<DataEntity> data;
    private CoordsEntity coords;

    public void setData(List<DataEntity> data) {
        this.data = data;
    }

    public void setCoords(CoordsEntity coords) {
        this.coords = coords;
    }

    public List<DataEntity> getData() {
        return data;
    }

    public CoordsEntity getCoords() {
        return coords;
    }

    public class DataEntity {
        /**
         * spaces_to_rent : 2
         * distance : {"km":0.27300765921247,"mile":0.16963909462998}
         * display_price : {"formatted_price":"£25.00","period":"day","price":25}
         * available : true
         * title : Driveway on Oakington Manor Drive, HA9
         * photos : {"user_added":[{"created_at":"2014-06-03 22:00:58","pid":59940,"is_verified":1,"deleted_at":null,"json_data":{"small":{"width":350,"url":"/media/uploaded/listing-photos/538e45c089fec-small.jpg","height":350},"normal":{"width":800,"url":"/media/uploaded/listing-photos/538e45c089fec-normal.jpg","height":600},"square":{"width":50,"url":"/media/uploaded/listing-photos/538e45c089fec-square.jpg","height":50},"thumb":{"width":120,"url":"/media/uploaded/listing-photos/538e45c089fec-thumb.jpg","height":120}},"uid":null,"review_date":"2014-06-04 15:07:20","original_filename":"/media/uploaded/raw/IMG_0128.JPG","is_deleted":0,"updated_at":null,"for_review":0,"id":21503,"timestamp":"2014-06-04 15:07:20"}],"google_map":"https://maps.googleapis.com/maps/api/staticmap?size=640x640&center=51.553457%2C-0.280456&sensor=true&key=AIzaSyB2bEpYuQa2bDYdfP14jJb4qLCwO2SBm24&zoom=15&maptype=roadmap&visual_refresh=true&markers=51.553457%2C-0.280456","google_streetview":"/media/uploaded/listing-photos/538e45c089fec-square.jpg"}
         * feedback : {"rating":4.78,"count":16}
         * currency : {"symbol":"£","code":"GBP"}
         * href : /1.1/listings/oakington-manor-drive-wembley-ha9-p538e44da823c8/
         * instant_bookings : 1
         * category : null
         * facilities : ["Overnight parking"]
         * is_api_enabled : 0
         * coords : {"lng":-0.280456,"lat":51.553457}
         */
        private int spaces_to_rent;
        private DistanceEntity distance;
        private Display_priceEntity display_price;
        private boolean available;
        private String title;
        private PhotosEntity photos;
        private FeedbackEntity feedback;
        private CurrencyEntity currency;
        private String href;
        private int instant_bookings;
        private String category;
        private List<String> facilities;
        private int is_api_enabled;
        private CoordsEntity coords;

        public void setSpaces_to_rent(int spaces_to_rent) {
            this.spaces_to_rent = spaces_to_rent;
        }

        public void setDistance(DistanceEntity distance) {
            this.distance = distance;
        }

        public void setDisplay_price(Display_priceEntity display_price) {
            this.display_price = display_price;
        }

        public void setAvailable(boolean available) {
            this.available = available;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setPhotos(PhotosEntity photos) {
            this.photos = photos;
        }

        public void setFeedback(FeedbackEntity feedback) {
            this.feedback = feedback;
        }

        public void setCurrency(CurrencyEntity currency) {
            this.currency = currency;
        }

        public void setHref(String href) {
            this.href = href;
        }

        public void setInstant_bookings(int instant_bookings) {
            this.instant_bookings = instant_bookings;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public void setFacilities(List<String> facilities) {
            this.facilities = facilities;
        }

        public void setIs_api_enabled(int is_api_enabled) {
            this.is_api_enabled = is_api_enabled;
        }

        public void setCoords(CoordsEntity coords) {
            this.coords = coords;
        }

        public int getSpaces_to_rent() {
            return spaces_to_rent;
        }

        public DistanceEntity getDistance() {
            return distance;
        }

        public Display_priceEntity getDisplay_price() {
            return display_price;
        }

        public boolean isAvailable() {
            return available;
        }

        public String getTitle() {
            return title;
        }

        public PhotosEntity getPhotos() {
            return photos;
        }

        public FeedbackEntity getFeedback() {
            return feedback;
        }

        public CurrencyEntity getCurrency() {
            return currency;
        }

        public String getHref() {
            return href;
        }

        public int getInstant_bookings() {
            return instant_bookings;
        }

        public String getCategory() {
            return category;
        }

        public List<String> getFacilities() {
            return facilities;
        }

        public int getIs_api_enabled() {
            return is_api_enabled;
        }

        public CoordsEntity getCoords() {
            return coords;
        }

        public class DistanceEntity {
            /**
             * km : 0.27300765921247
             * mile : 0.16963909462998
             */
            private double km;
            private double mile;

            public void setKm(double km) {
                this.km = km;
            }

            public void setMile(double mile) {
                this.mile = mile;
            }

            public double getKm() {
                return km;
            }

            public double getMile() {
                return mile;
            }
        }

        public class Display_priceEntity {
            /**
             * formatted_price : £25.00
             * period : day
             * price : 25
             */
            private String formatted_price;
            private String period;
            private double price;

            public void setFormatted_price(String formatted_price) {
                this.formatted_price = formatted_price;
            }

            public void setPeriod(String period) {
                this.period = period;
            }

            public void setPrice(double price) {
                this.price = price;
            }

            public String getFormatted_price() {
                return formatted_price;
            }

            public String getPeriod() {
                return period;
            }

            public double getPrice() {
                return price;
            }
        }

        public class PhotosEntity {
            /**
             * user_added : [{"created_at":"2014-06-03 22:00:58","pid":59940,"is_verified":1,"deleted_at":null,"json_data":{"small":{"width":350,"url":"/media/uploaded/listing-photos/538e45c089fec-small.jpg","height":350},"normal":{"width":800,"url":"/media/uploaded/listing-photos/538e45c089fec-normal.jpg","height":600},"square":{"width":50,"url":"/media/uploaded/listing-photos/538e45c089fec-square.jpg","height":50},"thumb":{"width":120,"url":"/media/uploaded/listing-photos/538e45c089fec-thumb.jpg","height":120}},"uid":null,"review_date":"2014-06-04 15:07:20","original_filename":"/media/uploaded/raw/IMG_0128.JPG","is_deleted":0,"updated_at":null,"for_review":0,"id":21503,"timestamp":"2014-06-04 15:07:20"}]
             * google_map : https://maps.googleapis.com/maps/api/staticmap?size=640x640&center=51.553457%2C-0.280456&sensor=true&key=AIzaSyB2bEpYuQa2bDYdfP14jJb4qLCwO2SBm24&zoom=15&maptype=roadmap&visual_refresh=true&markers=51.553457%2C-0.280456
             * google_streetview : /media/uploaded/listing-photos/538e45c089fec-square.jpg
             */
            private List<User_addedEntity> user_added;
            private String google_map;
            private String google_streetview;

            public void setUser_added(List<User_addedEntity> user_added) {
                this.user_added = user_added;
            }

            public void setGoogle_map(String google_map) {
                this.google_map = google_map;
            }

            public void setGoogle_streetview(String google_streetview) {
                this.google_streetview = google_streetview;
            }

            public List<User_addedEntity> getUser_added() {
                return user_added;
            }

            public String getGoogle_map() {
                return google_map;
            }

            public String getGoogle_streetview() {
                return google_streetview;
            }

            public class User_addedEntity {
                /**
                 * created_at : 2014-06-03 22:00:58
                 * pid : 59940
                 * is_verified : 1
                 * deleted_at : null
                 * json_data : {"small":{"width":350,"url":"/media/uploaded/listing-photos/538e45c089fec-small.jpg","height":350},"normal":{"width":800,"url":"/media/uploaded/listing-photos/538e45c089fec-normal.jpg","height":600},"square":{"width":50,"url":"/media/uploaded/listing-photos/538e45c089fec-square.jpg","height":50},"thumb":{"width":120,"url":"/media/uploaded/listing-photos/538e45c089fec-thumb.jpg","height":120}}
                 * uid : null
                 * review_date : 2014-06-04 15:07:20
                 * original_filename : /media/uploaded/raw/IMG_0128.JPG
                 * is_deleted : 0
                 * updated_at : null
                 * for_review : 0
                 * id : 21503
                 * timestamp : 2014-06-04 15:07:20
                 */
                private String created_at;
                private int pid;
                private int is_verified;
                private String deleted_at;
                private Json_dataEntity json_data;
                private String uid;
                private String review_date;
                private String original_filename;
                private int is_deleted;
                private String updated_at;
                private int for_review;
                private int id;
                private String timestamp;

                public void setCreated_at(String created_at) {
                    this.created_at = created_at;
                }

                public void setPid(int pid) {
                    this.pid = pid;
                }

                public void setIs_verified(int is_verified) {
                    this.is_verified = is_verified;
                }

                public void setDeleted_at(String deleted_at) {
                    this.deleted_at = deleted_at;
                }

                public void setJson_data(Json_dataEntity json_data) {
                    this.json_data = json_data;
                }

                public void setUid(String uid) {
                    this.uid = uid;
                }

                public void setReview_date(String review_date) {
                    this.review_date = review_date;
                }

                public void setOriginal_filename(String original_filename) {
                    this.original_filename = original_filename;
                }

                public void setIs_deleted(int is_deleted) {
                    this.is_deleted = is_deleted;
                }

                public void setUpdated_at(String updated_at) {
                    this.updated_at = updated_at;
                }

                public void setFor_review(int for_review) {
                    this.for_review = for_review;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public void setTimestamp(String timestamp) {
                    this.timestamp = timestamp;
                }

                public String getCreated_at() {
                    return created_at;
                }

                public int getPid() {
                    return pid;
                }

                public int getIs_verified() {
                    return is_verified;
                }

                public String getDeleted_at() {
                    return deleted_at;
                }

                public Json_dataEntity getJson_data() {
                    return json_data;
                }

                public String getUid() {
                    return uid;
                }

                public String getReview_date() {
                    return review_date;
                }

                public String getOriginal_filename() {
                    return original_filename;
                }

                public int getIs_deleted() {
                    return is_deleted;
                }

                public String getUpdated_at() {
                    return updated_at;
                }

                public int getFor_review() {
                    return for_review;
                }

                public int getId() {
                    return id;
                }

                public String getTimestamp() {
                    return timestamp;
                }

                public class Json_dataEntity {
                    /**
                     * small : {"width":350,"url":"/media/uploaded/listing-photos/538e45c089fec-small.jpg","height":350}
                     * normal : {"width":800,"url":"/media/uploaded/listing-photos/538e45c089fec-normal.jpg","height":600}
                     * square : {"width":50,"url":"/media/uploaded/listing-photos/538e45c089fec-square.jpg","height":50}
                     * thumb : {"width":120,"url":"/media/uploaded/listing-photos/538e45c089fec-thumb.jpg","height":120}
                     */
                    private SmallEntity small;
                    private NormalEntity normal;
                    private SquareEntity square;
                    private ThumbEntity thumb;

                    public void setSmall(SmallEntity small) {
                        this.small = small;
                    }

                    public void setNormal(NormalEntity normal) {
                        this.normal = normal;
                    }

                    public void setSquare(SquareEntity square) {
                        this.square = square;
                    }

                    public void setThumb(ThumbEntity thumb) {
                        this.thumb = thumb;
                    }

                    public SmallEntity getSmall() {
                        return small;
                    }

                    public NormalEntity getNormal() {
                        return normal;
                    }

                    public SquareEntity getSquare() {
                        return square;
                    }

                    public ThumbEntity getThumb() {
                        return thumb;
                    }

                    public class SmallEntity {
                        /**
                         * width : 350
                         * url : /media/uploaded/listing-photos/538e45c089fec-small.jpg
                         * height : 350
                         */
                        private int width;
                        private String url;
                        private int height;

                        public void setWidth(int width) {
                            this.width = width;
                        }

                        public void setUrl(String url) {
                            this.url = url;
                        }

                        public void setHeight(int height) {
                            this.height = height;
                        }

                        public int getWidth() {
                            return width;
                        }

                        public String getUrl() {
                            return url;
                        }

                        public int getHeight() {
                            return height;
                        }
                    }

                    public class NormalEntity {
                        /**
                         * width : 800
                         * url : /media/uploaded/listing-photos/538e45c089fec-normal.jpg
                         * height : 600
                         */
                        private int width;
                        private String url;
                        private int height;

                        public void setWidth(int width) {
                            this.width = width;
                        }

                        public void setUrl(String url) {
                            this.url = url;
                        }

                        public void setHeight(int height) {
                            this.height = height;
                        }

                        public int getWidth() {
                            return width;
                        }

                        public String getUrl() {
                            return url;
                        }

                        public int getHeight() {
                            return height;
                        }
                    }

                    public class SquareEntity {
                        /**
                         * width : 50
                         * url : /media/uploaded/listing-photos/538e45c089fec-square.jpg
                         * height : 50
                         */
                        private int width;
                        private String url;
                        private int height;

                        public void setWidth(int width) {
                            this.width = width;
                        }

                        public void setUrl(String url) {
                            this.url = url;
                        }

                        public void setHeight(int height) {
                            this.height = height;
                        }

                        public int getWidth() {
                            return width;
                        }

                        public String getUrl() {
                            return url;
                        }

                        public int getHeight() {
                            return height;
                        }
                    }

                    public class ThumbEntity {
                        /**
                         * width : 120
                         * url : /media/uploaded/listing-photos/538e45c089fec-thumb.jpg
                         * height : 120
                         */
                        private int width;
                        private String url;
                        private int height;

                        public void setWidth(int width) {
                            this.width = width;
                        }

                        public void setUrl(String url) {
                            this.url = url;
                        }

                        public void setHeight(int height) {
                            this.height = height;
                        }

                        public int getWidth() {
                            return width;
                        }

                        public String getUrl() {
                            return url;
                        }

                        public int getHeight() {
                            return height;
                        }
                    }
                }
            }
        }

        public class FeedbackEntity {
            /**
             * rating : 4.78
             * count : 16
             */
            private double rating;
            private int count;

            public void setRating(double rating) {
                this.rating = rating;
            }

            public void setCount(int count) {
                this.count = count;
            }

            public double getRating() {
                return rating;
            }

            public int getCount() {
                return count;
            }
        }

        public class CurrencyEntity {
            /**
             * symbol : £
             * code : GBP
             */
            private String symbol;
            private String code;

            public void setSymbol(String symbol) {
                this.symbol = symbol;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public String getSymbol() {
                return symbol;
            }

            public String getCode() {
                return code;
            }
        }

        public class CoordsEntity {
            /**
             * lng : -0.280456
             * lat : 51.553457
             */
            private double lng;
            private double lat;

            public void setLng(double lng) {
                this.lng = lng;
            }

            public void setLat(double lat) {
                this.lat = lat;
            }

            public double getLng() {
                return lng;
            }

            public double getLat() {
                return lat;
            }
        }
    }

    public class CoordsEntity {
        /**
         * lng : -0.27959400
         * lat : 51.55585300
         */
        private String lng;
        private String lat;

        public void setLng(String lng) {
            this.lng = lng;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public String getLat() {
            return lat;
        }
    }
}
