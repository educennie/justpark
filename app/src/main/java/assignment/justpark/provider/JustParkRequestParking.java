package assignment.justpark.provider;

import android.content.Context;

import com.google.gson.Gson;

import assignment.justpark.model.JPLocation;

/**
 * Created by ecnieves on 23/04/2015.
 */
public class JustParkRequestParking extends JustParkRequest<JPLocation> {

    public JustParkRequestParking(Context context) {
        super(context);
    }

    @Override
    public void performRequest(ResponseCallback<JPLocation> responseCallback) {
        super.performRequest(responseCallback);
    }

    @Override
    protected JPLocation getDataProcessed(String httpResponse) throws DataProcessedException {

        if (httpResponse == null) {
            throw new DataProcessedException("Invalid response");
        }

        Gson gson = new Gson();

        JPLocation jpLocation;

        try {
            jpLocation = gson.fromJson(httpResponse, JPLocation.class);
        } catch (Exception e) {
            throw new DataProcessedException(e);
        }

        return jpLocation;
    }
}
