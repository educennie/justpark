package assignment.justpark.ui.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import assignment.justpark.R;
import assignment.justpark.model.JPLocation;
import assignment.justpark.provider.JustParkRequestParking;
import assignment.justpark.provider.ResponseCallback;
import assignment.justpark.utils.LocationUtils;

/**
 * Created by educennie on 4/24/15.
 */
public class ListParkingFragment extends android.app.Fragment {

    public static ListParkingFragment newInstance() {
        ListParkingFragment fragment = new ListParkingFragment();

        // Set arguments.
        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);


        final View view = inflater.inflate(R.layout.fragment_list_parking, container, false);

        return view;
    }
}
