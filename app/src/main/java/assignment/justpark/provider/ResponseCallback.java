package assignment.justpark.provider;

/**
 * Created by ecnieves on 23/04/2015.
 */
public interface ResponseCallback<T> {


    /**
     * Success response callback.
     *
     * @param object the object
     */
    public void success(T object);

    /**
     * Failure response callback.
     *
     * @param exception the exception
     */
    public void failure(Exception exception);
}
