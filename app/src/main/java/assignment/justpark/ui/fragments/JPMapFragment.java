package assignment.justpark.ui.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;


import java.util.HashMap;
import java.util.List;

import assignment.justpark.R;
import assignment.justpark.model.JPLocation;
import assignment.justpark.utils.LocationUtils;

/**
 * Created by educennie on 4/21/15.
 */
public class JPMapFragment extends MapFragment implements LocationListener, GoogleMap.CancelableCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {



    private Context mContext;
    private OnCameraMoveListener onCameraMoveListener = null;
    private HashMap<Marker, JPLocation.DataEntity> markersItems = new HashMap<Marker, JPLocation.DataEntity>();

    // Stores the current instantiation of the location client in this object
//    private LocationClient mLocationClient;
    private GoogleApiClient mGoogleApiClient;


    // A request to connect to Location Services
    private LocationRequest mLocationRequest;
    private GoogleMap googleMap;
    private LatLng latLng;
    private Location lastLocationKnown;
//    private HashMap<Marker, UKMapDataItem> markersItems = new HashMap<Marker, UKMapDataItem>();


    private LatLng latLngDestination;
    private boolean locationEnabled = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mContext = getActivity();

        return super.onCreateView(inflater, container, savedInstanceState);
    }


    public void enableLocationMap() {
        // Getting GoogleMap object from the fragment
        googleMap = getMap();

        if (googleMap == null) {
            return;
        }

        // Enabling MyLocation Layer of Google Map
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);

        /*
        * Create a new location client, using the enclosing class to
        * handle callbacks.
        */
//        mLocationClient = new LocationClient(getActivity(), this, this);

        // Create a GoogleApiClient instance
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        /*
         * Connect the client. Don't re-start any requests here;
         * instead, wait for onResume()
         */
        if (mGoogleApiClient != null)
            mGoogleApiClient.connect();

        // Create a new global location parameters object
        mLocationRequest = LocationRequest.create();

        /*
         * Set the update interval
         */
        mLocationRequest.setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS);

        // Use high accuracy
        mLocationRequest.setPriority(LocationRequest.PRIORITY_NO_POWER);

        // Set the interval ceiling to one minute
        mLocationRequest.setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
    }



    @Override
    public void onFinish() {
        // Zoom in the Google Map
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(LocationUtils.ZOOM));
    }

    @Override
    public void onCancel() {
    }

    /*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(Bundle bundle) {
        startPeriodicUpdates();
        getLocation();
    }

    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {

                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(getActivity(), LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

                /*
                * Thrown if Google Play services canceled the original
                * PendingIntent
                */

            } catch (IntentSender.SendIntentException e) {

                // Log the error
                e.printStackTrace();
            }
        } else {

            // If no resolution is available, display a dialog to the user with the error.
            showErrorDialog(connectionResult.getErrorCode());
        }
    }


    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onConnectionSuspended(int i) {

    }

    /*
     * Handle results returned to this Activity by other Activities started with
     * startActivityForResult(). In particular, the method onConnectionFailed() in
     * LocationUpdateRemover and LocationUpdateRequester may call startResolutionForResult() to
     * start an Activity that handles Google Play services problems. The result of this
     * call returns here, to onActivityResult.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {

        // Choose what to do based on the request code
        switch (requestCode) {

            // If the request code matches the code sent in onConnectionFailed
            case LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST:

                switch (resultCode) {
                    // If Google Play services resolved the problem
                    case Activity.RESULT_OK:


                        break;

                    // If any other result was returned by Google Play services
                    default:
                        // Log the result

                        break;
                }

                // If any other request code was received
            default:
                // Report that this Activity received an unknown requestCode

                break;
        }
    }


    /**
     * Verify that Google Play services is available before making a request.
     *
     * @return true if Google Play services is available, otherwise false
     */
    private boolean servicesConnected() {

        // Check that Google Play services is available
        int resultCode =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());

        // If Google Play services is available
        if (ConnectionResult.SUCCESS == resultCode) {
            // In debug mode, log the status
            Log.d("Connection", "success");

            // Continue
            return true;
            // Google Play services was not available for some reason
        } else {
            // Display an error dialog
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, getActivity(), 0);
            if (dialog != null) {
                Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();

            }
            return false;
        }
    }

    /**
     * Calls getLastLocation() to get the current location
     */
    public void getLocation() {

        // If Google Play Services is available
        if (servicesConnected()) {

            // Get the current location
            Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            lastLocationKnown = currentLocation;

            if (currentLocation != null) {
                // Getting latitude of the current location
                double latitude = currentLocation.getLatitude();

                // Getting longitude of the current location
                double longitude = currentLocation.getLongitude();

                // Creating a LatLng object for the current location
                latLng = new LatLng(latitude, longitude);
                moveCameraToMyCurrentPosition();
            }
        }
    }


    /*
     * Called when the Activity is no longer visible at all.
     * Stop updates and disconnect.
     */
    @Override
    public void onStop() {
        if (mGoogleApiClient != null) {
            // If the client is connected
            if (mGoogleApiClient.isConnected()) {
                stopPeriodicUpdates();
            }

            // After disconnect() is called, the client is considered "dead".
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    /**
     * @param location The updated location.
     */
    @Override
    public void onLocationChanged(Location location) {

        // Getting latitude of the current location
        double latitude = location.getLatitude();

        // Getting longitude of the current location
        double longitude = location.getLongitude();

        // Creating a LatLng object for the current location
        latLng = new LatLng(latitude, longitude);

//        if(isNavigationFragment){
//            stopPeriodicUpdates();
//            moveCameraToMyCurrentPosition();
//            if(!locationEnabled) {
//                locationEnabled = true;
//                Log.d("onLocationChanged - ", "start");
//                getDirections(getActivity(), LocationUtils.TRAVEL_MODE_WALKING);
//            }
//
//        }
    }

    /**
     * In response to a request to start updates, send a request
     * to Location Services
     */
    private void startPeriodicUpdates() {
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    /**
     * In response to a request to stop updates, send a request to
     * Location Services
     */
    private void stopPeriodicUpdates() {
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    /**
     * Show a dialog returned by Google Play services for the
     * connection error code
     *
     * @param errorCode An error code returned from onConnectionFailed
     */
    private void showErrorDialog(int errorCode) {

        // Get the error dialog from Google Play services
        Dialog errorDialog = GooglePlayServicesUtil.getErrorDialog(
                errorCode,
                getActivity(),
                LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);

        // If Google Play services can provide an error dialog
        if (errorDialog != null) {

            Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
        }
    }

    public void addMarkersAround(List<? extends JPLocation.DataEntity> mapList){

        if(googleMap==null) {
            return;
        }

        for(HashMap.Entry<Marker , JPLocation.DataEntity> entry : markersItems.entrySet()){
            entry.getKey().remove();
        }
//        googleMap.clear();
        markersItems.clear();
        for(JPLocation.DataEntity jpMapItem : mapList){
            Marker marker = googleMap.addMarker(getMarker(new LatLng(jpMapItem.getCoords().getLat(), jpMapItem.getCoords().getLng()),
                     R.drawable.justpark_marker));
            markersItems.put(marker, jpMapItem);
        }

        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                JPLocation.DataEntity markers = markersItems.get(marker);
//                if(onMarkerClickListener!=null){
//                    onMarkerClickListener.OnMarker(ukMapDataItem);
//                }
//
//                changeMarkerSelected(marker);


                return false;
            }
        });
    }

    public MarkerOptions getMarker(LatLng point, int defaultMarkerIconDrawableId) {

            return new MarkerOptions()
                    .position(point)
                    .icon(BitmapDescriptorFactory.fromResource(defaultMarkerIconDrawableId));

    }



    public void moveCameraToPosition(final LatLng coordinates) {
        moveCameraToPosition(coordinates, false);
    }

    public void moveCameraToPosition(final LatLng coordinates, boolean isMarkerSelected) {
        moveCameraToPosition(coordinates, isMarkerSelected, LocationUtils.ZOOM);
    }

    public void moveCameraToPosition(final LatLng coordinates, boolean isMarkerSelected, final float zoomCamera) {

        if(googleMap==null)
            return;

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                CameraUpdate zoom = CameraUpdateFactory.newLatLngZoom(coordinates, zoomCamera);
                googleMap.animateCamera(zoom);
            }
        }, 300);

        if(onCameraMoveListener != null) {
            onCameraMoveListener.OnCameraMove(coordinates, isMarkerSelected);
        }
    }


    public void moveCameraToMyCurrentPosition() {
        if(latLng!=null) {
            googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng), LocationUtils.TIME_ANIMATION, this);
            if(onCameraMoveListener != null)
                onCameraMoveListener.OnCameraMove(latLng, false);

        }else{
            if(lastLocationKnown != null)
                googleMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(lastLocationKnown.getLatitude(), lastLocationKnown.getLongitude())), LocationUtils.TIME_ANIMATION, this);        }
    }

    public interface OnCameraMoveListener {
        public abstract void OnCameraMove(LatLng coordinates, boolean isMarkerSelected);
    }

    public void setOnCameraMoveListener(OnCameraMoveListener listener) {
        onCameraMoveListener = listener;
    }
}